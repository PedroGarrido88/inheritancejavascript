/**
 * Create Payment Form
 *
 */

//Payment Options
var method1 = 'PayPal',
    method2 = 'Bank Draft',
    method3 = 'Postal Order',
    method4 = 'Credit Card';

//Create PaymentMethod Objects
var paymentMethod1 = new PaymentMethod(method1),
    paymentMethod2 = new PaymentMethod(method2),
    paymentMethod3 = new PaymentMethod(method3);

//Create CreditCardPaymentMethod Object
var paymentCreditCard = new CreditCardPaymentMethod(method4);
