/**
 * PaymentMethod Class
 */

/**
 * Constructor of PaymentMethod
 * @param paymentMethodId
 * @constructor
 */
function PaymentMethod(paymentMethodId) {
    var that = this;
    that._init(paymentMethodId);
}

/**
 * Init Method
 * @param paymentMethodId
 * @private
 */
PaymentMethod.prototype._init = function (paymentMethodId) {
    this.type = paymentMethodId;
    this._calculateFee();
    this._initView(paymentMethodId);
};

/**
 * Initialize View
 * @private
 */
PaymentMethod.prototype._initView = function () {
    this._generateView();
};

/**
 * Returns if this option of payment was selected
 * @returns {boolean}
 */
PaymentMethod.prototype.isSelected = function () {
    return document.getElementById(this.type).childNodes[0].checked;
};

/**
 *  Returns total of fee, applying the suplement percent
 */
PaymentMethod.prototype.getTotalFee = function () {
    var priceText = this.totalFee + ' Euros ',
        suplementText = '(' + this.suplementPercent + '%)';
    document.getElementById('totalFee').innerHTML = priceText + suplementText;
};


/**
 * Returns basic price
 * @returns {number}
 */
PaymentMethod.prototype.getTotalFeeWithoutDiscounts = function () {
    var basicPrice = 25;
    return basicPrice;
};

/**
 * Calculate total fee of any payment method, except credit card payment
 * @private
 */
PaymentMethod.prototype._calculateFee = function () {
    var basicFee = this.getTotalFeeWithoutDiscounts();
    this.suplementPercent = Math.floor((Math.random() * 100) + 1);
    this.totalFee = (basicFee * this.suplementPercent) / 100;
};

/**
 * Calculate total fee of credit card payment method
 * @private
 */
PaymentMethod.prototype.getTotalFeeCredit = function (typeCreditCard) {
    var basicFee = this.getTotalFeeWithoutDiscounts();
    var suplementPercent = this.getSuplementCreditCard(typeCreditCard);
    var totalFee = (basicFee * suplementPercent) / 100;


    var priceText = totalFee + ' Euros ',
        suplementText = '(' + suplementPercent + '%)';
    document.getElementById('totalFee').innerHTML = priceText + suplementText;
};

/**
 * Return suplement percent of specific credit card
 * @param typeCredit
 * @returns {*}
 */
PaymentMethod.prototype.getSuplementCreditCard = function (typeCredit) {
    var suplementPercent;
    switch (typeCredit) {
        case 'visa':
            suplementPercent = 15;
            break;
        case 'mastercard':
            suplementPercent = 20;
            break;

        case 'europaExpress':
            suplementPercent = 25;
            break;
    }
    return suplementPercent;
};

/**
 * Create view (HTML) and add event listeners
 * @private
 */
PaymentMethod.prototype._generateView = function () {
    var that = this;
    var type = this.type;

    if (this.type !== 'Credit Card') {
        that.view = '<label id="' + type + '" class="center-block">' +
        '<input type="radio" name="options" autocomplete="off" checked>' +
        type +
        '</label>';
    } else {
        that.view = '<label id="' + type + '" class="center-block">' +
        '<input type="radio" name="options" autocomplete="off" checked>' +
        type +
        '<select id="creditcard-select" class="center-block">' +
        '<option  value="visa">Visa</option>' +
        '<option  value="mastercard">Mastercard</option>' +
        '<option value="europaExpress">Europa Express</option>' +
        '</select>' +
        '</label>';
        type = "creditcard-select";
    }

    $(that.view).appendTo("#formPaymentMethod");
    document.getElementById(type).addEventListener("click", function (item) {

        if (item.currentTarget.value) {
            that.getTotalFeeCredit(item.currentTarget.value);
        } else {
            that.getTotalFee();
        }
    });

};

/*
 PaymentMethod.prototype._generateViewDomObject = function() {
 var divDOM = document.createElement("LABEL");
 var inputDOM = document.createElement("INPUT");
 inputDOM.setAttribute("type", "radio");
 inputDOM.setAttribute("id", this.type);
 inputDOM.setAttribute("autocomplete", "off");
 divDOM.appendChild(inputDOM);
 divDOM.appendChild(document.createTextNode(this.type));
 document.getElementById('formPaymentMethod').appendChild(divDOM);
 };*/
