# Exercise about inheritance object in JS#

En el siguiente documento se explican cómo se han abordado los puntos críticos de la prueba técnica.

### Create objects ###


```
#!javascript

//Create PaymentMethod Object
var paymentMethod1 = new PaymentMethod('Paypal');

//Create CreditCardPaymentMethod Object
var paymentCreditCard = new CreditCardPaymentMethod('Visa');
```

### Constructor ###

```
#!javascript

function PaymentMethod(paymentMethodId) {
  this._init(paymentMethodId);
}

PaymentMethod.prototype._init = function(paymentMethodId) {
  this._initView(paymentMethodId);
};

PaymentMethod.prototype._initView = function() {
  this._generateView();
};

```

### Inheritance ###

```
#!javascript
function CreditCardPaymentMethod(paymentMethodId) {
  CreditCardPaymentMethod.prototype = new PaymentMethod('Credit Card');
}

CreditCardPaymentMethod.prototype._init = function (paymentMethodId) {
    this.type = paymentMethodId;
    PaymentMethod.apply(CreditCardPaymentMethod.prototype, [paymentMethodId]);
};
```

### Views ###
```
#!html
<!-- Paymethod View-->
<label id="PayPal" class="center-block">
  <input type="radio" name="options" autocomplete="off" checked="">PayPal
</label>

<!-- CreditCard Paymethod View-->
<label id="creditCard" class="center-block">
  <input type="radio" name="options" autocomplete="off" checked="">Credit Card

  <select id="creditcard-select" class="center-block">
    <option value="visa">Visa</option>
    <option value="mastercard">Mastercard</option>
    <option value="europaExpress">Europa Express</option>
  </select>
</label>
```

### Functional Objectives ###

* Seleccionar un método de pago y poder comprobar mediante su instancia si está seleccionado y saber su precio suplementario.


```
#!javascript

PaymentMethod.prototype.isSelected = function() {
  return document.getElementById(this.type).childNodes[0].checked;
};


PaymentMethod.prototype.getTotalFee = function(item) {
  if (item.currentTarget.value) {
    this._getTotalFeeCredit(item.currentTarget.value);
  } else {
    this._getTotalFeePaymentMethod();
  }
};

```

* En el caso de las tarjetas de crédito, además saber que tipo de tarjeta está
seleccionada.


```
#!javascript
CreditCardPaymentMethod.prototype.getCreditCardSelected = function () {
    return document.getElementsByTagName('SELECT')[0].value;
};


```

### Total Fee of PaymentMethod ###

```
#!javascript
PaymentMethod.prototype._calculateFee = function() {
  var basicFee = this.getTotalFeeWithoutDiscounts();
  this.suplementPercent = Math.floor((Math.random() * 100) + 1);
  this.totalFee = (basicFee * this.suplementPercent) / 100;
};

PaymentMethod.prototype._getTotalFeePaymentMethod = function(typeCreditCard) {
  var priceText = this.totalFee + ' Euros ',
    suplementText = '(' + this.suplementPercent + '%)';
  document.getElementById('totalFee').innerHTML = priceText + suplementText;
};

```


### Total Fee of CreditCard PaymentMethod ###
```
#!javascript

PaymentMethod.prototype._getTotalFeeCreditCard = function(typeCreditCard) {
  var basicFee = this.getTotalFeeWithoutDiscounts();
  var suplementPercent = this._getSuplementCreditCard(typeCreditCard);
  var totalFee = (basicFee * suplementPercent) / 100;


  var priceText = totalFee + ' Euros ',
    suplementText = '(' + suplementPercent + '%)';
  document.getElementById('totalFee').innerHTML = priceText + suplementText;
};

PaymentMethod.prototype._getSuplementCreditCard = function(typeCredit) {
  var suplementPercent;
  switch (typeCredit) {
    case 'visa':
      suplementPercent = 15;
      break;
    case 'mastercard':
      suplementPercent = 20;
      break;

    case 'europaExpress':
      suplementPercent = 25;
      break;
  }
  return suplementPercent;
};
```