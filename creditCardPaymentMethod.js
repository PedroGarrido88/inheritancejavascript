/**
 * CreditCardPaymentMethod Class
 */

/**
 * Constructor of CreditCardPaymentMethod
 * @param paymentMethodId
 * @constructor
 */
function CreditCardPaymentMethod(paymentMethodId) {

    //Type of credit cards
    var creditCard1 = 'Visa',
        creditCard2 = 'Mastercard',
        creditCard3 = 'Europa Express';

    //Inherited of PaymentMethod
    CreditCardPaymentMethod.prototype = new PaymentMethod('Credit Card');
}

/**
 *  Init Method
 * @param paymentMethodId
 * @private
 */
CreditCardPaymentMethod.prototype._init = function (paymentMethodId) {
    this.type = paymentMethodId;

    //super of constructor of PaymentMethod Class
    PaymentMethod.apply(CreditCardPaymentMethod.prototype, [paymentMethodId]);
};

/**
 *  Returns name of credit card that was selected
 * @returns {string|Number}
 */
CreditCardPaymentMethod.prototype.getCreditCardSelected = function () {
    return document.getElementsByTagName('SELECT')[0].value;
};

/**
 * Check if the this payment method is selected
 * @returns {boolean}
 */
CreditCardPaymentMethod.prototype.isSelected = function () {
    return CreditCardPaymentMethod.prototype.isSelected();
};
